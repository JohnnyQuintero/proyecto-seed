/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.util.Objects;

/**
 *
 * @author JHONY QUINTERO
 */
public class Persona implements Comparable {

    private String Nombre;
    private int id;

    public Persona() {
    }

    public Persona(String Nombre, int id) {
        this.Nombre = Nombre;
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Nombre = " + Nombre + " id = " + id + "\n";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.Nombre);
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.id != other.id) {
            return false;
        }
        final Persona otro = (Persona) obj;
        if (this.id != otro.id) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        final Persona otro = (Persona) o;
        return this.id - otro.id;
    }
}
